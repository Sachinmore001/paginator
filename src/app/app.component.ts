import { Component, OnInit } from '@angular/core';
import { PaginatorDefaultOptions } from 'projects/paginator/src/public-api';
import { Page } from 'paginator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'paginator-app';
  paginatorDefaultOptions: PaginatorDefaultOptions;
  page: Page;


  rows = [
    {
      id: 1,
      name: 'sachin',
      address: 'solapur'
    },
    {
      id: 2,
      name: 'rahul',
      address: 'sangola'
    },
    {
      id: 3,
      name: 'sagar',
      address: 'pune'
    },
    {
      id: 4,
      name: 'akshay',
      address: 'sangli'
    },
    {
      id: 5,
      name: 'raj',
      address: 'satara'
    },
    {
      id: 6,
      name: 'vijay',
      address: 'pune'
    },
    {
      id: 7,
      name: 'vikas',
      address: 'solapur'
    },
    {
      id: 8,
      name: 'rakesh',
      address: 'hyd'
    },
    {
      id: 9,
      name: 'suraj',
      address: 'solapur'
    },
    {
      id: 10,
      name: 'ashok',
      address: 'hyd'
    },
    {
      id: 11,
      name: 'yesh',
      address: 'solapur'
    },
    {
      id: 12,
      name: 'tejas',
      address: 'mumbai'
    },
    {
      id: 13,
      name: 'kedar',
      address: 'akluj'
    },
    {
      id: 14,
      name: 'venkata',
      address: 'pune'
    },
    {
      id: 15,
      name: 'ashish',
      address: 'pune'
    },
    {
      id: 16,
      name: 'sachin',
      address: 'solapur'
    },
    {
      id: 17,
      name: 'rahul',
      address: 'sangola'
    },
    {
      id: 18,
      name: 'sagar',
      address: 'pune'
    },
    {
      id: 19,
      name: 'akshay',
      address: 'sangli'
    },
    {
      id: 20,
      name: 'raj',
      address: 'satara'
    },
    {
      id: 21,
      name: 'vijay',
      address: 'pune'
    },
    {
      id: 22,
      name: 'vikas',
      address: 'solapur'
    },
    {
      id: 23,
      name: 'rakesh',
      address: 'hyd'
    },
    {
      id: 24,
      name: 'suraj',
      address: 'solapur'
    },
    {
      id: 25,
      name: 'ashok',
      address: 'hyd'
    },
    {
      id: 26,
      name: 'yesh',
      address: 'solapur'
    },
    {
      id: 27,
      name: 'tejas',
      address: 'mumbai'
    },
    {
      id: 28,
      name: 'kedar',
      address: 'akluj'
    },
    {
      id: 29,
      name: 'venkata',
      address: 'pune'
    },
    {
      id: 30,
      name: 'ashish',
      address: 'pune'
    }
  ];


  ngOnInit() {
    this.paginatorDefaultOptions = {
      pageIndex: 0,
      pageSize: 10,
      pageSizeOptions: [5, 10, 25, 100],
      totalElements: 0
    };
  }


  pageEvent(pageEvent) {
    this.page = pageEvent;
    this.paginatorDefaultOptions.pageIndex = this.page.offset;
    this.paginatorDefaultOptions.pageSize = this.page.size;
    this.paginatorDefaultOptions.totalElements = this.page.count;
  }

  setCount(count) {
    console.log('set count: ', count);
    this.paginatorDefaultOptions = {
      pageIndex: 0,
      pageSize: 10,
      pageSizeOptions: [5, 10, 25, 100],
      totalElements: count
    };
  }


  /* ====================================================== */

  tabChanged(event) {
    console.log('event tab: ', event);
  }
}
