import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PaginatorModule } from 'projects/paginator/src/public-api';
import { TabGroupModule } from 'projects/tab-group/src/public-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PaginatorModule,
    TabGroupModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
