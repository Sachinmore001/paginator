import { NgModule } from '@angular/core';
import { PaginatorComponent } from './paginator.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [PaginatorComponent],
  imports: [
    BrowserModule,
    CommonModule
  ],
  exports: [PaginatorComponent]
})
export class PaginatorModule { }
