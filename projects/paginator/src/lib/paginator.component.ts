import { Component, Input, Output, EventEmitter, HostListener, OnChanges } from '@angular/core';

export interface PaginatorDefaultOptions {
  pageIndex?: number;
  pageSize?: number;
  pageSizeOptions?: number[];
  totalElements?: number;
  totalPages?: number;
}

export class Page {
  count: number;
  offset: number;
  size: number;
  startIndex: number;
  endIndex: number;
  rangeLabel: any;
}

@Component({
  selector: 'ng-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnChanges {
  @Input() paginatorDefaultOptions: PaginatorDefaultOptions;
  @Output() pageEvent: EventEmitter<any> = new EventEmitter();
  populatePageSizeOptions = false;
  insideClickFlag = false;
  page: any;
  pageIndexArray: any[];
  activePageLink = 1;

  constructor() {}

  ngOnChanges(): void {
     this.page = new Page();
     this.page.count = this.paginatorDefaultOptions.totalElements;
     this.page.offset = this.paginatorDefaultOptions.pageIndex;
     this.page.size = this.paginatorDefaultOptions.pageSize;
     this.page.startIndex = 0;
     this.page.endIndex = 0;
     this.page.rangeLabel = `0 of ${this.paginatorDefaultOptions.totalElements}`;
     this.pageSize(this.paginatorDefaultOptions.pageSize);
  }

  @HostListener('click')
  insideClick() {
    this.insideClickFlag = true;
  }

  @HostListener('document: click')
  outsideClick() {
    if (!this.insideClickFlag) {
      this.populatePageSizeOptions = false;
    }
    this.insideClickFlag = false;
  }

  /** Move back to the previous page if it exists. */
  previousPage() {
    if (this.page.offset >= 1) {
      this.page.offset = this.page.offset - 1;
      this.getRangeLabel(this.page.offset, this.page.size, this.page.count);
      this.pageEvent.emit(this.page);
    }
  }

  /** Advances to the next page if it exists. */
  nextPage() {
    if (this.page.endIndex !== this.page.count) {
      this.page.offset = this.page.offset + 1;
      this.getRangeLabel(this.page.offset, this.page.size, this.page.count);
      this.pageEvent.emit(this.page);
    }
  }

  /** Method for changing page size */
  pageSize(pageSize) {
    this.paginatorDefaultOptions.totalPages = Math.ceil(
      this.paginatorDefaultOptions.totalElements / pageSize
    );
    this.page.offset = (this.page.offset < this.paginatorDefaultOptions.totalPages)
    ? this.page.offset
    : this.paginatorDefaultOptions.totalPages - 1;
    this.page.size = pageSize;
    this.populatePageSizeOptions = false;
    this.getRangeLabel(this.page.offset, this.page.size, this.page.count);
    this.pageEvent.emit(this.page);
  }

  /** A label for the range of items within the current page and the length of the whole list. */
  getRangeLabel(page: number, pageSize: number, length: number) {
    this.pageIndexArray = [];
    this.activePageLink = (page === 0) ? 1 : page + 1;
    if (length === 0 || pageSize === 0) {
      this.page.startIndex = 0;
      this.page.endIndex = 0;
      this.page.rangeLabel = `0 of ${length}`;
    } else {
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = (startIndex < length)
          ? Math.min(startIndex + pageSize, length)
          : startIndex + pageSize;
      this.page.startIndex = startIndex + 1;
      this.page.endIndex = endIndex;
      const pageEvent = page + 1;
      const prevIndex = (pageEvent === this.paginatorDefaultOptions.totalPages)
          ? pageEvent - 2
          : pageEvent - 1;
      const prev = (pageEvent === 1) ? pageEvent : prevIndex;
      const next = (pageEvent === 1) ? pageEvent + 2 : pageEvent + 1;
      for (let i = prev; i <= next; i++) {
        if (i > 0 && i <= this.paginatorDefaultOptions.totalPages) {
          this.pageIndexArray.push(i);
        }
      }
      this.page.rangeLabel = `${startIndex + 1} – ${endIndex} of ${length}`;
    }
    this.pageEvent.emit(this.page);
  }

  getPageLink(pageIndex: any) {
    this.page.offset = pageIndex - 1;
    this.getRangeLabel(this.page.offset, this.page.size, this.page.count);
  }

  /** Method for showing page size options */
  openPageSizeOptions() {
    this.populatePageSizeOptions = true;
  }
}
