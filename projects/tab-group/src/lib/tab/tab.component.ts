import { Component, OnInit, Input, ElementRef, QueryList, ContentChildren } from '@angular/core';
import { TabTitleDirective } from '../tab-title.directive';

@Component({
  selector: 'qf-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() label = '';

  /** Content for the tab label given by `qfTabTitle` directive. */
  @ContentChildren(TabTitleDirective, {read: ElementRef}) tabLabel: QueryList<ElementRef>;
    constructor() {}

  ngOnInit(): void {
  }
}
