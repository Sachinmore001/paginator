import { NgModule } from '@angular/core';
import { TabGroupComponent } from './tab-group.component';
import { TabTitleDirective } from './tab-title.directive';
import { TabContentDirective } from './tab-content.directive';
import { BrowserModule } from '@angular/platform-browser';
import { TabComponent } from './tab/tab.component';

@NgModule({
  declarations: [
    TabGroupComponent,
    TabTitleDirective,
    TabContentDirective,
    TabComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    TabGroupComponent,
    TabTitleDirective,
    TabContentDirective,
    TabComponent


  ]
})
export class TabGroupModule { }
