import { Component, OnInit, QueryList, ContentChildren, ElementRef, AfterViewInit } from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'qf-tab-group',
  templateUrl: './tab-group.component.html',
  styleUrls: ['./tab-group.component.scss']
})
export class TabGroupComponent implements OnInit, AfterViewInit {

  @ContentChildren(TabComponent, {read: ElementRef}) allTabs: QueryList<ElementRef>;
  selectedTabIndex: any;
  allTabTitle: any[];
  allTabContent: any;

  constructor( ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    /*  this.allTabs.forEach(tabInstance => {
      console.log(tabInstance.nativeElement.childNodes[0].innerText);
      console.log(tabInstance.nativeElement.childNodes[1].innerHTML);
    }); */
    const titles = [] = this.allTabs.map(tab => tab.nativeElement.attributes.label ?.value);
    this.allTabTitle = (titles.includes(undefined))
     ? this.allTabs.map(tab => tab.nativeElement.childNodes[0].innerText)
     : titles;
    console.log('all tabs: ', this.allTabs);
    console.log('text context: ', this.allTabs.map(tab => tab.nativeElement.childNodes[2]?.outerHTML));
    console.log('inner text: ', this.allTabs.map(tab => tab.nativeElement.childNodes[2]?.textContent));
    const outerTabContent = [] = this.allTabs.map(tab => tab.nativeElement.childNodes[2]?.textContent);
    const innerTabContent = [] = this.allTabs.map(tab => tab.nativeElement.childNodes[2]?.textContent);
   // this.allTabContent = this.allTabs.map(tab => tab.nativeElement.childNodes[1].innerHTML);

    if (!innerTabContent.includes(undefined)) {
      this.allTabContent = innerTabContent;
    } else if (!outerTabContent.includes(undefined)) {
      this.allTabContent = outerTabContent;
    } else {
      this.allTabContent = this.allTabs.map(tab => tab.nativeElement.childNodes[1].innerHTML);
    }
  }

  getTabIndex(index) {
    this.selectedTabIndex = index;
    }
}
