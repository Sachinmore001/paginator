/*
 * Public API Surface of tab-group
 */


export * from './lib/tab-group.service';
export * from './lib/tab-group.component';
export * from './lib/tab-group.module';
export * from './lib/tab-title.directive';
export * from './lib/tab-content.directive';
export * from './lib/tab/tab.component';
